<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class DBGrid {

    private $controller;

    public function __construct($params) {
        // Do something with $params
        $this->controller = $params[0];
    }

    public function render($name, $config_script = NULL, $edit_menu = NULL) {
        $component_data['grid_name'] = $name;
        if (isset($config_script)) {
            $this->controller->jquery->addScript(ON_DOCUMENT_READY, $config_script);
        }
        if (isset($edit_menu)) {
            $component_data['grid_edit_menu'] = $this->controller->load->view($edit_menu, NULL, true);
        }
        $this->controller->jquery->addScript(ON_DOCUMENT_READY, 'components-jquery/dbgrid.js', $component_data);
        return $this->controller->load->view('components/dbgrid', $component_data, true);
    }

}

?>
