<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class DBDropDownList {

    private $controller;

    public function __construct($params) {
        // Do something with $params
        $this->controller = $params[0];
    }

    public function render($name, $config_script = NULL, $event_script = NULL) {
        $component_data['name'] = $name;
        if (isset($config_script)) {
            $this->controller->jquery->addScript(ON_DOCUMENT_READY, $config_script);
        }
        $this->controller->jquery->addScript(ON_DOCUMENT_READY, 'components-jquery/dbdropdownlist.js', $component_data);
        if (isset($event_script)) {
            $this->controller->jquery->addScript(ON_DOCUMENT_READY, $event_script);
        }
        return $this->controller->load->view('components/dbdropdownlist', $component_data, true);
    }

}

?>
