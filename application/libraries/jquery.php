<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

define("ON_DOCUMENT_READY", "on_document_ready");

class JQuery {

    private $controller;
    private $content;

    public function __construct($params) {
        // Do something with $params
        $this->controller = $params[0];
        $this->content[ON_DOCUMENT_READY] = '';
    }

    public function render() {
        $data['jquery_content'] = $this->controller->load->view('components/jquery', $this->content, true);
        return $data;
    }

    public function add($element, $content) {
        $this->content[$element] .= $content;
    }

    public function addScript($element, $script, $data = NULL) {
        $this->content[$element] .= $this->controller->load->view($script, $data, true);
    }

}

?>