<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu {

    private $controller;

    public function __construct($params) {
        // Do something with $params
        $this->controller = $params[0];
    }

    public function render($data, $items) {
        $data['menu'] = $this->controller->load->view('components/menu', array('items' => $items), true);
        return $data;
    }

}

?>