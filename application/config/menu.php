<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$config['menu'] = array(
    array('title' => 'Daftar', 'uri' => '#', 'type' => 'dropdown', 'items' =>
        array(
            array('title' => 'Pelanggan', 'uri' => 'lists/clients', 'type' => 'link'),
            array('title' => 'Penjual', 'uri' => 'lists/salespersons', 'type' => 'link'),
            array('title' => 'Proyek', 'uri' => 'lists/projects', 'type' => 'link'),
            array('title' => 'Pemasok', 'uri' => 'lists/suppliers', 'type' => 'link'),
            array('title' => 'Persediaan', 'uri' => 'lists/supply', 'type' => 'link')
        ),
    ),
    array('title' => 'Penjualan', 'uri' => '#', 'type' => 'dropdown', 'items' =>
        array(
            array('title' => 'Penawaran Penjualan', 'uri' => 'sales/proposal', 'type' => 'link'),
            array('title' => 'Kontrak Penjualan', 'uri' => 'sales/orders', 'type' => 'link'),
            array('title' => 'Faktur Penjualan', 'uri' => 'sales/invoice', 'type' => 'link')
        ),
    ),
    array('title' => 'Laporan', 'uri' => '#', 'type' => 'dropdown', 'items' =>
        array(
            array('title' => 'Sales Invoice', 'uri' => 'sales/invoice', 'type' => 'link')
        ),
    ),
);
?>
