<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Supply extends MY_Model {

    var $table_name = 'supply';
    var $primary_key = 'id';

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function get($name = NULL) {
        $query = $this->db->query("
            SELECT 
                IR.*, 
                U1.name as width_unit, 
                U2.name as height_unit, 
                U3.name as length_unit, 
                U4.name as sale_price_unit
            FROM $this->table_name IR
            LEFT JOIN unit U1
            ON IR.width_unit_id = U1.id
            LEFT JOIN unit U2
            ON IR.height_unit_id = U2.id
            LEFT JOIN unit U3
            ON IR.length_unit_id = U3.id
            LEFT JOIN unit U4
            ON IR.sale_price_unit_id = U4.id
            ORDER BY $this->primary_key asc
        ");
        return $query->result_array();
    }

    public function create($data) {
        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where(array($this->primary_key => $id));
        return $this->db->update($this->table_name, $data);
    }

    public function delete($id) {
        return $this->db->delete($this->table_name, array($this->primary_key => $id));
    }

}