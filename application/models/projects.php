<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Projects extends MY_Model {

    var $table_name = 'projects';
    var $primary_key = 'id';

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function get($name = NULL) {
        $this->db->select("*");
        $this->db->from($this->table_name);
        if (!is_null($name)) {
            $this->db->where(array('name' => $name));
        }
        $this->db->order_by($this->primary_key, "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function create($data) {
        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where(array($this->primary_key => $id));
        return $this->db->update($this->table_name, $data);
    }

    public function delete($id) {
        return $this->db->delete($this->table_name, array($this->primary_key => $id));
    }

}