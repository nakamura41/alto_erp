<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sales extends MY_Model {

    var $table_name = 'sales';
    var $primary_key = 'id';

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function get($name = NULL) {
        $this->db->select("*");
        $this->db->from($this->table_name);
        if (!is_null($name)) {
            $this->db->where(array('name' => $name));
        }
        $this->db->order_by($this->primary_key, "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

}