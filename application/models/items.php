<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Items extends MY_Model {

    var $table_name = 'items';
    var $primary_key = 'id';

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function get($name = NULL) {
        $query = $this->db->query("
            SELECT 
                IR.*, 
                U1.name as unit
            FROM $this->table_name IR
            LEFT JOIN unit U1
            ON IR.unit_id = U4.id
            ORDER BY $this->primary_key asc
        ");
        return $query->result_array();
    }

    public function create($data) {
        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        $this->db->where(array($this->primary_key => $id));
        return $this->db->update($this->table_name, $data);
    }

    public function delete($id) {
        return $this->db->delete($this->table_name, array($this->primary_key => $id));
    }

}