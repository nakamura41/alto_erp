<form>
    <div class="row-fluid">
        <div class="span12">
            <h2>Penawaran Penjualan</h2>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span7">
            <div class="row-fluid">
                <div class="span1">
                    Kepada
                </div>
                <div class="span6">
                    <?php echo $client_dbinput; ?>
                    <a id="client-add" class="btn btn-primary" type="button" href="<?php echo base_url(); ?>lists/clients" target="blank"><i class="icon-plus icon-white"></i> Tambah</a>
                </div>
            </div>
            <div class="row-fluid" style="display:block; height:10px"></div>
            <div class="row-fluid">
                <div class="span1">
                    Alamat
                </div>
                <div class="span6">
                    <input id="client_address" type="text" style="width:350px"/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span1">
                    Kota
                </div>
                <div class="span3">
                    <input id="client_city" type="text" style="width:150px"/>
                </div>
                <div class="span1">
                    Kd.Pos
                </div>
                <div class="span2">
                    <input id="client_postalcode" type="text" style="width:90px"/>
                </div>
            </div>
        </div>
        <div class="span5" style="text-align: right">
            <div class="row-fluid">
                Penjual&nbsp;<?php echo $salesperson_dbinput; ?>
                <a id="salesperson-add" class="btn btn-primary" type="button" href="<?php echo base_url(); ?>lists/salespersons" target="blank"><i class="icon-plus icon-white"></i> Tambah</a>
            </div>
            <div class="row-fluid" style="display:block; height:10px"></div>
            <div class="row-fluid">
                Proyek&nbsp;<?php echo $project_dbinput; ?>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <?php echo $dbgrid1; ?>
        </div>
    </div>
</form>
