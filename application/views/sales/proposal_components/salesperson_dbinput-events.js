$(components['salesperson_dbinput']['dom_class_name']).on('select', function(event) {
    if (event.args) {
        var item = event.args.item;
        if (item) {
            var data = components['salesperson_dbinput']['data'];
            $('#salesperson_id').val(data[item.value].id);
        }
    }
});