if (typeof components == 'undefined') {
    components = [];
}

components['client_dbinput'] = {
    'id_name': 'client_id',
    'dataHandler': {
        "get": "<?php echo base_url() . 'datasource/clients_get'; ?>"
    },
    'datafields': [
        {name: 'id'},
        {name: 'name'},
        {name: 'address'},
        {name: 'city'},
        {name: 'postal_code'}
    ],
    'label': "Nama Pelanggan",
    'width': '248px',
    'height': '28px',
    'display_field': 'name',
    'value_field': 'id',
    'events': {
        'update': null
    }
};
