if (typeof components == 'undefined') {
    components = [];
}

components['project_dbinput'] = {
    'id_name': 'project_id',
    'dataHandler': {
        "get": "<?php echo base_url() . 'datasource/projects_get'; ?>"
    },
    'datafields': [
        {name: 'id'},
        {name: 'name'},
        {name: 'address'},
        {name: 'city'},
        {name: 'postal_code'}
    ],
    'label': "Nama Proyek",
    'width': '300px',
    'height': '28px',
    'display_field': 'name',
    'value_field': 'id',
    'events': {
        'update': null
    }
};
