$(components['client_dbinput']['dom_class_name']).on('select', function(event) {
    if (event.args) {
        var item = event.args.item;
        if (item) {
            var selectedData = components['client_dbinput']['data'][item.value];
            $('#client_id').val(selectedData.id);
            $('#client_address').val(selectedData.address);
            $('#client_city').val(selectedData.city);
            $('#client_postalcode').val(selectedData.postal_code);
        }
    }
});