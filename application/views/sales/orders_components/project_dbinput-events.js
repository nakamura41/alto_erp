$(components['project_dbinput']['dom_class_name']).on('select', function(event) {
    if (event.args) {
        var item = event.args.item;
        if (item) {
            var selectedData = components['project_dbinput']['data'][item.value];
            $('#project_id').val(selectedData.id);
        }
    }
});