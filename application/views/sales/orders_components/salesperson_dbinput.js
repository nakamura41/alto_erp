if (typeof components == 'undefined') {
    components = [];
}

components['salesperson_dbinput'] = {
    'id_name': 'salesperson_id',
    'dataHandler': {
        "get": "<?php echo base_url() . 'datasource/salespersons_get'; ?>"
    },
    'datafields': [
        {name: 'id'},
        {name: 'name'},
        {name: 'address'},
        {name: 'city'},
        {name: 'postal_code'}
    ],
    'label': "Nama Penjual",
    'display_field': 'name',
    'value_field': 'id',
    'width': '200px',
    'height': '28px'
};