<div class="alert-container">
    <div id="<?php echo $grid_name; ?>_alert" class="alert" >
        <a class="close" href="#">&times;</a>
        <span></span>
    </div>
</div>
<div id="<?php echo $grid_name; ?>"></div>
<div style="margin-top: 30px;">
    <div id="cellbegineditevent"></div>
    <div style="margin-top: 10px;" id="cellendeditevent"></div>
</div>
<div id="<?php echo $grid_name; ?>_popup_window">
    <div>Edit</div>
    <div style="overflow: hidden;">
        <table>
            <?php echo $grid_edit_menu; ?>
            <tr>
                <td align="right"></td>
                <td style="padding-top: 10px;" align="right" colspan='2'>
                    <input style="margin-right: 5px;" type="button" id="<?php echo $grid_name; ?>_create_button" value="Simpan" />
                    <input style="margin-right: 5px;" type="button" id="<?php echo $grid_name; ?>_save_button" value="Ubah" />
                    <input type="button" id="<?php echo $grid_name; ?>_cancel_button" value="Batal" />
                </td>
            </tr>
        </table>
    </div>
</div>
<!-- pop up menu -->
<div id="<?php echo $grid_name; ?>_menu">
    <ul>
        <li>Create New Data</li>
        <li>Edit Selected Data</li>
        <li>Delete Selected Data</li>
    </ul>
</div>
