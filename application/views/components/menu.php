<?php $items_count = count($items); ?>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <?php for ($i = 0; $i < $items_count; $i++) { ?>
                    <span class="icon-bar"></span>
                <?php } ?>
            </button>
            <a class="brand" href="<?php echo base_url(); ?>"><?php echo COMPANY_NAME; ?></a>
            <div class="nav-collapse collapse">
                <ul class="nav">
                    <?php foreach ($items as $item) { ?>
                        <?php if (strcmp($item['type'], 'link') == 0) { ?>
                            <li><?php echo anchor($item['uri'], $item['title']); ?></li>
                        <?php } elseif (strcmp($item['type'], 'dropdown') == 0) { ?>
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><?php echo $item['title']; ?><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <?php $subitems = $item['items']; ?>
                                    <?php foreach ($subitems as $subitem) { ?>
                                        <li><?php echo anchor($subitem['uri'], $subitem['title']); ?></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div>