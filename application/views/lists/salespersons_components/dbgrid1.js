var dbgrid_config = [];
dbgrid_config['name'] = 'dbgrid1';
dbgrid_config['data_get'] = "<?php echo base_url() . 'datasource/salespersons_get'; ?>";
dbgrid_config['data_add'] = "<?php echo base_url() . 'datasource/salespersons_create'; ?>";
dbgrid_config['data_update'] = "<?php echo base_url() . 'datasource/salespersons_update'; ?>";
dbgrid_config['data_delete'] = "<?php echo base_url() . 'datasource/salespersons_delete'; ?>";

dbgrid_config['events'] = {
    'clearInputElements': null,
    'fillInputElements': null,
    'retrieveDataFromInputElements': null,
    'focusOnFirstInputElement': null,
    'refreshData': null,
    'showAddDataForm': null,
    'showUpdateDataForm': null,
    'deleteData': null
};

dbgrid_config['datafields'] = [
    {name: 'id', type: 'int'},
    {name: 'name'},
    {name: 'address'},
    {name: 'city'},
    {name: 'postal_code'},
    {name: 'phone'},
    {name: 'email'},
];

dbgrid_config['columns'] = [
    {text: 'ID', datafield: 'id', width: '30', editable: false},
    {text: 'Nama', datafield: 'name', columntype: 'textbox', filtertype: 'textbox'},
    {text: 'Alamat', datafield: 'address', columntype: 'textbox', filtertype: 'textbox'},
    {text: 'Kota', datafield: 'city', columntype: 'textbox', filtertype: 'textbox'},
    {text: 'Kode Pos', datafield: 'postal_code', columntype: 'textbox', filtertype: 'textbox'},
    {text: 'No Telp', datafield: 'phone', columntype: 'textbox', filtertype: 'textbox'},
    {text: 'Email', datafield: 'email', columntype: 'textbox', filtertype: 'textbox'},
];

// initialize the input fields.
var standard_input_height = '23px';
$("#id").jqxInput({theme: theme, height: standard_input_height, width: '200px', disabled: true});
$("#name").jqxInput({theme: theme, height: standard_input_height, width: '200px'});
$("#address").jqxInput({theme: theme, height: standard_input_height, width: '200px'});
$("#city").jqxInput({theme: theme, height: standard_input_height, width: '200px'});
$("#postal_code").jqxInput({theme: theme, height: standard_input_height, width: '200px'});
$("#phone").jqxInput({theme: theme, height: standard_input_height, width: '200px'});
$("#email").jqxInput({theme: theme, height: standard_input_height, width: '200px'});

dbgrid_config['events']['clearInputElements'] = function() {
    $("#id").val(null);
    $("#name").val(null);
    $("#address").val(null);
    $("#city").val(null);
    $("#postal_code").val(null);
    $("#phone").val(null);
    $("#email").val(null);
}

dbgrid_config['events']['fillInputElements'] = function(dataRecord) {
    $("#id").val(dataRecord.id);
    $("#name").val(dataRecord.name);
    $("#address").val(dataRecord.address);
    $("#city").val(dataRecord.city);
    $("#postal_code").val(dataRecord.postal_code);
    $("#phone").val(dataRecord.phone);
    $("#email").val(dataRecord.email);
}

dbgrid_config['events']['retrieveDataFromInputElements'] = function() {
    return {
        id: $("#id").val(),
        name: $("#name").val(),
        address: $("#address").val(),
        city: $("#city").val(),
        postal_code: $("#postal_code").val(),
        phone: $("#phone").val(),
        email: $("#email").val(),
    };
}

dbgrid_config['events']['focusOnFirstInputElement'] = function() {
    $("#name").jqxInput('selectAll');
}