$(components['width_unit']['dom_class_name']).on('select', function(event) {
    if (event.args) {
        var item = event.args.item;
        if (item) {
            $('#width_unit_id').val(item.value);
        }
    }
});
;
