<tr>
    <td align="right">ID:</td>
    <td align="left" colspan="2"><input id="id"/></td>
</tr>
<tr>
    <td align="right">Nama:</td>
    <td align="left" colspan="2"><input id="name"/></td>
</tr>
<tr>
    <td align="right">Warna:</td>
    <td align="left" colspan="2"><input id="color"/></td>
</tr>
<tr>
    <td align="right">Lebar:</td>
    <td align="left">
        <div id="width" style="display: inline-block"></div>
        <div id="width_unit" style="display: inline-block;"></div>
        <div id="width_unit_id"></div>
    </td>
</tr>
<tr>
    <td align="right">Tinggi:</td>
    <td align="left">
        <div id="height" style="display: inline-block"></div>
        <div id="height_unit" style="display: inline-block;"></div>
        <div id="height_unit_id"></div>
    </td>
</tr>
<tr>
    <td align="right">Panjang:</td>
    <td align="left">
        <div id="length" style="display: inline-block"></div>
        <div id="length_unit" style="display: inline-block;"></div>
        <div id="length_unit_id"></div>
    </td>
</tr>
<tr>
    <td align="right">Harga per unit:</td>
    <td align="left">
        <div id="sale_price_per_unit" style="display: inline-block"></div>
        <div id="sale_price_unit" style="display: inline-block;"></div>
        <div id="sale_price_unit_id"></div>
    </td>
</tr>