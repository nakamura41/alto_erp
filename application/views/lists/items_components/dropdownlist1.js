if (typeof components == 'undefined') {
    components = [];
}

components['unit'] = {
    'dataHandler': {
        "get": "<?php echo base_url() . 'datasource_static/unit_get'; ?>"
    },
    'datafields': [
        {name: 'id'},
        {name: 'name'}
    ],
    'width': '60px',
    'height': '23px',
    'display_field': 'name',
    'value_field': 'id',
    'events': {
        'update': null
    }
};
