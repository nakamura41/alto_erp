var dbgrid_config = [];
dbgrid_config['name'] = 'dbgrid1';
dbgrid_config['data_get'] = "<?php echo base_url() . 'datasource/items_get'; ?>";
dbgrid_config['data_add'] = "<?php echo base_url() . 'datasource/items_create'; ?>";
dbgrid_config['data_update'] = "<?php echo base_url() . 'datasource/items_update'; ?>";
dbgrid_config['data_delete'] = "<?php echo base_url() . 'datasource/items_delete'; ?>";

dbgrid_config['events'] = {
    'clearInputElements': null,
    'fillInputElements': null,
    'retrieveDataFromInputElements': null,
    'focusOnFirstInputElement': null,
    'refreshData': null,
    'showAddDataForm': null,
    'showUpdateDataForm': null,
    'deleteData': null
};

dbgrid_config['datafields'] = [
    {name: 'id', type: 'int'},
    {name: 'name'},
    {name: 'color'},
    {name: 'width'},
    {name: 'width_unit_id'},
    {name: 'width_unit'},
    {name: 'height'},
    {name: 'height_unit_id'},
    {name: 'height_unit'},
    {name: 'length'},
    {name: 'length_unit_id'},
    {name: 'length_unit'},
    {name: 'sale_price_per_unit', type: 'number'},
    {name: 'unit_id'},
    {name: 'sale_price_unit'},
    {name: 'sale_price_unit_id'}
];

dbgrid_config['columns'] = [
    {text: 'ID', datafield: 'id', width: '30', editable: false},
    {text: 'Nama', datafield: 'name', columntype: 'textbox', filtertype: 'textbox'},
    {text: 'Warna', datafield: 'color', columntype: 'textbox', filtertype: 'textbox', width: '120'},
    {text: 'Lebar', datafield: 'width', columntype: 'numberinput', filtertype: 'textbox', cellsalign: 'right', width: '60'},
    {text: 'Satuan', datafield: 'width_unit', width: '50'},
    {text: 'Tinggi', datafield: 'height', columntype: 'numberinput', filtertype: 'textbox', cellsalign: 'right', width: '60'},
    {text: 'Satuan', datafield: 'height_unit', width: '50'},
    {text: 'Panjang', datafield: 'length', columntype: 'numberinput', filtertype: 'textbox', cellsalign: 'right', width: '60'},
    {text: 'Satuan', datafield: 'length_unit', width: '50'},
    {text: 'Harga Produksi/Satuan', datafield: 'sale_price_per_unit', columntype: 'numberinput', filtertype: 'textbox', cellsalign: 'right', cellsformat: 'c0', width: '150'},
    {text: 'Satuan', datafield: 'sale_price_unit', width: '50'}
];

// initialize the input fields.
var standard_input_height = '23px';
$("#id").jqxInput({theme: theme, height: standard_input_height, width: '200px', disabled: true});
$("#name").jqxInput({theme: theme, height: standard_input_height, width: '200px'});
$("#color").jqxInput({theme: theme, height: standard_input_height, width: '135px'});
$("#width").jqxNumberInput({theme: theme, height: standard_input_height, width: '70px', inputMode: 'simple'});
$("#height").jqxNumberInput({theme: theme, height: standard_input_height, width: '70px', inputMode: 'simple'});
$("#length").jqxNumberInput({theme: theme, height: standard_input_height, width: '70px', inputMode: 'simple'});
$("#sale_price_per_unit").jqxNumberInput({theme: theme, height: standard_input_height, width: '130px', symbol: 'Rp', decimalDigits: 0, digits: 12});

dbgrid_config['events']['clearInputElements'] = function() {
    $("#id").val(null);
    $("#name").val(null);
    $("#color").val(null);
    $("#width").jqxNumberInput({'value': 0});
    $("#width_unit").jqxDropDownList({'selectedIndex': 0});
    $("#height").jqxNumberInput({'value': 0});
    $("#height_unit").jqxDropDownList({'selectedIndex': 0});
    $("#length").jqxNumberInput({'value': 0});
    $("#length_unit").jqxDropDownList({'selectedIndex': 0});
    $("#sale_price_per_unit").jqxNumberInput({'value': 0});
    $("#sale_price_unit").jqxDropDownList({'selectedIndex': 0});
}

dbgrid_config['events']['fillInputElements'] = function(dataRecord) {
    $("#id").val(dataRecord.id);

    $("#name").val(dataRecord.name);

    $("#color").val(dataRecord.color);

    $("#width").val(dataRecord.width);

    if (typeof dataRecord.width_unit_id == 'undefined')
        dataRecord.width_unit_id = 0;
    $("#width_unit").jqxDropDownList({'selectedIndex': dataRecord.width_unit_id - 1});

    $("#height").val(dataRecord.height);

    if (typeof dataRecord.height_unit_id == 'undefined')
        dataRecord.height_unit_id = 0;
    $("#height_unit").jqxDropDownList({'selectedIndex': dataRecord.height_unit_id - 1});

    $("#length").val(dataRecord.length);

    if (typeof dataRecord.length_unit_id == 'undefined')
        dataRecord.length_unit_id = 0;
    $("#length_unit").jqxDropDownList({'selectedIndex': dataRecord.length_unit_id - 1});

    $("#sale_price_per_unit").val(dataRecord.sale_price_per_unit);

    if (typeof dataRecord.sale_price_unit_id == 'undefined')
        dataRecord.sale_price_unit_id = 0;
    $("#sale_price_unit").jqxDropDownList({'selectedIndex': dataRecord.sale_price_unit_id - 1});
}

dbgrid_config['events']['retrieveDataFromInputElements'] = function() {
    return {
        id: $("#id").val(),
        name: $("#name").val(),
        color: $("#color").val(),
        width: $("#width").val(),
        width_unit_id: $("#width_unit_id").val(),
        height: $("#height").val(),
        height_unit_id: $("#height_unit_id").val(),
        length: $("#length").val(),
        length_unit_id: $("#length_unit_id").val(),
        sale_price_per_unit: $("#sale_price_per_unit").val(),
        sale_price_unit_id: $("#sale_price_unit_id").val()
    };
}

dbgrid_config['events']['focusOnFirstInputElement'] = function() {
    $("#name").jqxInput('selectAll');
}