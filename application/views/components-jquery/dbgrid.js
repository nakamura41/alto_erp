// prepare the data
var theme = "<?php echo $this->config->item('jqwidgets_theme'); ?>";

// grid localization
var localizationobj = {};
localizationobj.currencysymbol = "Rp";
localizationobj.decimalseparator = ",";
localizationobj.thousandsseparator = ".";

var class_prefix = '#';
if (typeof dbgrid_config['name'] == 'undefined') {
    dbgrid_config['name'] = 'dbgrid1';
}

dbgrid_config['dom_class_name'] = '#' + dbgrid_config['name'];
var dbgrid = $(dbgrid_config['dom_class_name']);

dbgrid.data('elements', {});
dbgrid.data('elements').rightClickMenu = $(dbgrid_config['dom_class_name'] + "_menu");
dbgrid.data('elements').contextMenu = dbgrid.data('elements').rightClickMenu.jqxMenu({width: 200, height: 87, autoOpenPopup: false, mode: 'popup', theme: theme});
dbgrid.data('elements').popUpWindow = $(dbgrid_config['dom_class_name'] + "_popup_window");
dbgrid.data('elements').alertBar = $(dbgrid_config['dom_class_name'] + "_alert");
dbgrid.data('elements').alertBar.span = $(dbgrid_config['dom_class_name'] + "_alert span");
dbgrid.data('elements').alertBar.closeButton = $(dbgrid_config['dom_class_name'] + '_alert .close');
dbgrid.data('elements').saveButton = $(dbgrid_config['dom_class_name'] + "_save_button").jqxButton({theme: theme});
dbgrid.data('elements').createButton = $(dbgrid_config['dom_class_name'] + "_create_button").jqxButton({theme: theme});
dbgrid.data('elements').cancelButton = $(dbgrid_config['dom_class_name'] + "_cancel_button").jqxButton({theme: theme});

// add close function for alert close button
dbgrid.data('elements').alertBar.closeButton.on('click', function() {
    $(this).parent().hide();
});

dbgrid_config['create_new_data_text'] = '';

// assign previously set events
dbgrid.data('events', dbgrid_config['events']);

// on refresh data event
dbgrid.data('events').refreshData = function() {
    dbgrid.jqxGrid('updatebounddata', 'cells');
};

// on show form event
dbgrid.data('events').showAddDataForm = function() {
    var rowindex = dbgrid.jqxGrid('getselectedrowindex');
    var x_pos = ($(window).width() - dbgrid.data('elements').popUpWindow.jqxWindow('width')) / 2 + $(window).scrollLeft();
    var y_pos = ($(window).height() - dbgrid.data('elements').popUpWindow.jqxWindow('height')) / 2 + $(window).scrollTop();
    dbgrid.data('elements').popUpWindow.jqxWindow({
        position: {x: x_pos, y: y_pos}
    });

    // clear all Input Elements on Form
    dbgrid.data('events').clearInputElements();

    // show create button
    dbgrid.data('elements').createButton.show();
    dbgrid.data('elements').saveButton.hide();

    // show the popup window.
    dbgrid.data('elements').popUpWindow.jqxWindow('show');
}

dbgrid.data('events').showUpdateDataForm = function() {
    var rowindex = dbgrid.jqxGrid('getselectedrowindex');
    var x_pos = ($(window).width() - dbgrid.data('elements').popUpWindow.jqxWindow('width')) / 2 + $(window).scrollLeft();
    var y_pos = ($(window).height() - dbgrid.data('elements').popUpWindow.jqxWindow('height')) / 2 + $(window).scrollTop();
    dbgrid.data('elements').popUpWindow.jqxWindow({
        position: {x: x_pos, y: y_pos}
    });

    var dataRecord = dbgrid.jqxGrid('getrowdata', rowindex);

    // fill all of the input box with data from a selected row
    dbgrid.data('events').fillInputElements(dataRecord);

    // show update button
    dbgrid.data('elements').saveButton.show();
    dbgrid.data('elements').createButton.hide();

    // show the popup window.
    dbgrid.data('elements').popUpWindow.jqxWindow('show');
}

dbgrid.data('events').deleteData = function() {
    // get the clicked row's data and initialize the input fields.
    var selectedrowindex = dbgrid.jqxGrid('getselectedrowindex');
    var rowscount = dbgrid.jqxGrid('getdatainformation').rowscount;
    var id = dbgrid.jqxGrid('getrowid', selectedrowindex);
    var commit = dbgrid.jqxGrid('deleterow', id);
}

dbgrid.data('dataSource', {
    url: dbgrid_config['data_get'],
    datafields: dbgrid_config['datafields'],
    datatype: "json",
    addrow: function(rowid, rowdata, position, commit) {
        $.ajax({
            async: false,
            type: "POST",
            url: dbgrid_config['data_add'],
            data: {id: rowid, data: rowdata}
        }).done(function(msg) {
            if (parseInt(msg) < 1) {
                dbgrid.data('elements').alertBar.removeClass('alert-info alert-success alert-error');
                dbgrid.data('elements').alertBar.addClass('alert-error');
                dbgrid.data('elements').alertBar.span.html('Data "' + rowdata.name + '" gagal dimasukkan!');
                dbgrid.data('elements').alertBar.show(400);
            } else {
                rowdata.id = parseInt(msg);
                dbgrid.data('elements').alertBar.removeClass('alert-info alert-success alert-error');
                dbgrid.data('elements').alertBar.addClass('alert-success');
                dbgrid.data('elements').alertBar.span.html('Data "' + rowdata.name + '" telah dimasukkan!');
                dbgrid.data('elements').alertBar.show(400);
            }
            commit(true);
            dbgrid.data('events').refreshData();
        });

    },
    updaterow: function(rowid, rowdata, commit) {
        $.ajax({
            async: false,
            type: "POST",
            url: dbgrid_config['data_update'],
            data: {id: rowdata.id, data: rowdata}
        }).done(function(msg) {
            if (msg == 'false') {
                dbgrid.data('elements').alertBar.removeClass('alert-info alert-success alert-error');
                dbgrid.data('elements').alertBar.addClass('alert-error');
                dbgrid.data('elements').alertBar.span.html('Data "' + rowdata.name + '" gagal diubah!');
                dbgrid.data('elements').alertBar.show(400);
            } else {
                dbgrid.data('elements').alertBar.removeClass('alert-info alert-success alert-error');
                dbgrid.data('elements').alertBar.addClass('alert-success');
                dbgrid.data('elements').alertBar.span.html('Data "' + rowdata.name + '" telah diubah!');
                dbgrid.data('elements').alertBar.show(400);
            }
            commit((msg == 'true'));
            dbgrid.data('events').refreshData();
        });

    },
    deleterow: function(rowid, commit) {
        var dataRecord = $(dbgrid_config['dom_class_name']).jqxGrid('getrowdata', rowid);
        $.ajax({
            async: false,
            type: "POST",
            url: dbgrid_config['data_delete'],
            data: {id: dataRecord.id}
        }).done(function(msg) {
            if (msg == 'false') {
                dbgrid.data('elements').alertBar.removeClass('alert-info alert-success alert-error');
                dbgrid.data('elements').alertBar.addClass('alert-error');
                dbgrid.data('elements').alertBar.span.html('Data "' + dataRecord.name + '" gagal dihapus!');
                dbgrid.data('elements').alertBar.show(400);
            } else {
                dbgrid.data('elements').alertBar.removeClass('alert-info alert-success alert-error');
                dbgrid.data('elements').alertBar.addClass('alert-info');
                dbgrid.data('elements').alertBar.span.html('Data "' + dataRecord.name + '" telah dihapus!');
                dbgrid.data('elements').alertBar.show(400);
            }
            commit((msg == 'true'));
        });
        dbgrid.data('events').refreshData();
    }
});

dbgrid.data("dataAdapter", new $.jqx.dataAdapter(dbgrid.data('dataSource'), {
    loadComplete: function(data) {
    },
    loadError: function(xhr, status, error) {
    }
}));

dbgrid.jqxGrid({
    width: '100%',
    theme: theme,
    sortable: true,
    showstatusbar: true,
    //selectionmode: 'singlecell',
    //editable: true,
    renderstatusbar: function(statusbar) {
        // appends buttons to the status bar.
        var container = $("<div style='overflow: hidden; position: relative;'></div>");
        var addButton = $("<div style='float: left; margin-left: 5px; margin-right: 2px;'><img style='position: relative;' src='<?php echo base_url(); ?>js/jqwidgets/resources/add.png'/><span style='margin-left: 4px; position: relative; top: 3px;'>Tambah</span></div>");
        var updateButton = $("<div style='float: left; margin-left: 5px; margin-right: 2px;'><img style='position: relative;' src='<?php echo base_url(); ?>js/jqwidgets/resources/tasksIcon.png'/><span style='margin-left: 4px; position: relative; top: 3px;'>Ubah</span></div>");
        var deleteButton = $("<div style='float: left; margin-left: 5px; margin-right: 2px;'><img style='position: relative; margin-top: 2px;' src='<?php echo base_url(); ?>js/jqwidgets/resources/close.png'/><span style='margin-left: 4px; position: relative; top: 3px;'>Hapus</span></div>");
        var refreshButton = $("<div style='float: left; margin-left: 5px; margin-right: 2px;'><img style='position: relative; margin-top: 2px;' src='<?php echo base_url(); ?>js/jqwidgets/resources/refresh.png'/><span style='margin-left: 4px; position: relative; top: 3px;'>Refresh</span></div>");
        container.append(addButton);
        container.append(updateButton);
        container.append(deleteButton);
        container.append(refreshButton);
        statusbar.append(container);
        addButton.jqxButton({theme: theme, width: 70, height: 20});
        updateButton.jqxButton({theme: theme, width: 70, height: 20});
        deleteButton.jqxButton({theme: theme, width: 70, height: 20});
        refreshButton.jqxButton({theme: theme, width: 70, height: 20});

        // create new row.
        addButton.on('click', function() {
            dbgrid.data('events').showAddDataForm();
        });
        // update existing row.
        updateButton.on('click', function() {
            dbgrid.data('events').showUpdateDataForm();
        });
        // delete existing row.
        deleteButton.on('click', function() {
            dbgrid.data('events').deleteData();
        });
        // delete existing row.
        refreshButton.on('click', function() {
            dbgrid.data('events').refreshData();
        });
    },
    source: dbgrid.data("dataAdapter"),
    columns: dbgrid_config['columns'],
    localization: localizationobj,
});

// hide alert message
dbgrid.data('elements').alertBar.hide();

// initialize the popup window and buttons.
dbgrid.data('elements').popUpWindow.jqxWindow({
    width: 280,
    resizable: false,
    theme: theme,
    isModal: true,
    autoOpen: false,
    cancelButton: dbgrid.data('elements').cancelButton,
    modalOpacity: 0.01
});

dbgrid.data('elements').popUpWindow.on('open',
        function() {
            dbgrid.data('events').focusOnFirstInputElement();
        }
);

// create new row when the user clicks the 'Create' button.
dbgrid.data('elements').createButton.click(
        function() {
            var rowdata = dbgrid.data('events').retrieveDataFromInputElements();
            dbgrid.jqxGrid('addrow', null, rowdata);
            dbgrid.data('elements').popUpWindow.jqxWindow('hide');
        }
);

// update the edited row when the user clicks the 'Save' button.
dbgrid.data('elements').saveButton.click(
        function() {
            var rowdata = dbgrid.data('events').retrieveDataFromInputElements();
            var rowindex = dbgrid.jqxGrid('getselectedrowindex');
            var rowID = $(dbgrid_config['dom_class_name']).jqxGrid('getrowid', rowindex);
            dbgrid.jqxGrid('updaterow', rowID, rowdata);
            dbgrid.data('elements').popUpWindow.jqxWindow('hide');
        }
);

// create context menu
dbgrid.on('contextmenu', function() {
    return false;
});

// handle context menu clicks.
dbgrid.data('elements').rightClickMenu.on('itemclick',
        function(event) {
            var args = event.args;

            if ($.trim($(args).text()) == "Create New Data") {
                dbgrid.data('events').showAddDataForm();
            } else if ($.trim($(args).text()) == "Edit Selected Data") {
                dbgrid.data('events').showUpdateDataForm();
            } else if ($.trim($(args).text()) == "Delete Selected Data") {
                dbgrid.data('events').deleteData();
            }
        }
);

dbgrid.on('rowclick',
        function(event) {
            if (event.args.rightclick) {
                dbgrid.jqxGrid('selectrow', event.args.rowindex);

                var scrollTop = $(window).scrollTop();
                var scrollLeft = $(window).scrollLeft();
                dbgrid.data('elements').rightClickMenu.jqxMenu('open', parseInt(event.args.originalEvent.clientX) + 5 + scrollLeft, parseInt(event.args.originalEvent.clientY) + 5 + scrollTop);
                return false;
            }
        }
);

