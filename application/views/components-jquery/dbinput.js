// prepare the data
var theme = "<?php echo $this->config->item('jqwidgets_theme'); ?>";
var name = "<?php echo $name; ?>";

var class_prefix = '#';
components[name]['dom_class_name'] = class_prefix + name;
components[name]['dom_id_class_name'] = class_prefix + components[name]['id_name'];

// data handler is defined
if (typeof components[name]['dataHandler'] != 'undefined') {
    components[name]['source'] = {
        datatype: "json",
        datafields: components[name]['datafields'],
        id: components[name]['id'],
        url: components[name]['dataHandler']['get'],
        async: false
    };

    components[name]['dataAdapter'] = new $.jqx.dataAdapter(components[name]['source'], {
        loadComplete: function(data) {
            components[name]['data'] = [];
            $.each(data, function(i, item) {
                components[name]['data'][item.id] = item;
            });
        },
        loadError: function(xhr, status, error) {
        }
    });
}
// Create a jqxDropDownList
$(components[name]['dom_class_name']).jqxInput({
    theme: theme,
    width: components[name]['width'],
    height: components[name]['height'],
});

// setup data source later
if (typeof components[name]['dataHandler'] != 'undefined') {
    $(components[name]['dom_class_name']).jqxInput({
        placeHolder: components[name]['label'],
        displayMember: components[name]['display_field'],
        valueMember: components[name]['value_field'],
        source: components[name]['dataAdapter'],
    });
}