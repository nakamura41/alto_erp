<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->


        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/main.css" type="text/css" >
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-responsive.css" type="text/css" >
        <link rel="stylesheet" href="<?php echo base_url(); ?>js/jqwidgets/styles/jqx.base.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>js/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />

        <script src="<?php echo base_url(); ?>js/jquery-1.10.1.min.js"></script>

        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxcore.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxbuttons.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxscrollbar.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxdata.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxlistbox.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxdropdownlist.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxmenu.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxgrid.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxinput.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxnumberinput.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxwindow.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxgrid.sort.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxgrid.filter.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxgrid.edit.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxgrid.selection.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxpanel.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxcalendar.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxdatetimeinput.js"></script>
        <script src="<?php echo base_url(); ?>js/jqwidgets/jqxgrid.pager.js"></script>

        <script src="<?php echo base_url(); ?>js/bootstrap-typeahead.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap-transition.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap-alert.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap-modal.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap-dropdown.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap-scrollspy.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap-tab.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap-tooltip.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap-popover.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap-button.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap-collapse.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap-carousel.js"></script>

        <?php echo $jquery_content; ?>
    </body>
    <link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" media="screen">
    <style>
        body {
            padding-top: 40px; /* 60px to make the container go all the way to the bottom of the topbar */
        }
    </style>
    <title>Karya Mandiri ERP</title> 
</head>
<body>
    <?php echo $menu; ?>
    <div class="container-fluid">
        <?php echo $content; ?>
    </div>
</html>