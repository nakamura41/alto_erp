<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    protected $layout;
    protected $data;

    public function __construct() {
        parent::__construct();
        $this->config->load('menu', TRUE, TRUE);
        
        $this->load->library('menu', array($this));
        $this->load->library('jquery', array($this));

        $this->data['jquery_content'] = NULL;
        $this->data['items'] = $this->config->item('menu', 'menu');

        $this->layout = 'default';
    }

    public function render($data = NULL, $view_name = NULL) {
        $callers = debug_backtrace();
        $function_name = (is_null($view_name) ? strtolower($callers[1]['function']) : $view_name);
        if (!is_null($data))
            $this->data = $data;

        $this->data = array_merge($this->data, $this->menu->render($this->data, $this->data['items']));
        $this->data['content'] = $this->load->view(strtolower($callers[1]['class']) . '/' . $function_name, $this->data, true);
        $this->load->view('layouts/' . $this->layout, $this->data);
    }

}