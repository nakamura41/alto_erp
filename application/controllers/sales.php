<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sales extends MY_Controller {

    public function index() {
        $this->render($this->data);
    }

    public function orders() {
        $this->load->library('dbinput', array($this));
        $this->data['client_dbinput'] = $this->dbinput->render('client_dbinput', 'Pelanggan', 'client_id', 'sales/orders_components/client_dbinput.js', 'sales/orders_components/client_dbinput-events.js');
        $this->data['salesperson_dbinput'] = $this->dbinput->render('salesperson_dbinput', 'Penjual', 'salesperson_id', 'sales/orders_components/salesperson_dbinput.js', 'sales/orders_components/salesperson_dbinput-events.js');
        $this->data['project_dbinput'] = $this->dbinput->render('project_dbinput', 'Proyek', 'project_id', 'sales/orders_components/project_dbinput.js', 'sales/orders_components/project_dbinput-events.js');
        $this->data = array_merge($this->data, $this->jquery->render());
        $this->render($this->data);
    }

    public function proposal() {
        $this->load->library('dbgrid', array($this));
        $this->load->library('dbinput', array($this));
        $this->data['dbgrid1'] = $this->dbgrid->render('dbgrid1', 'sales/proposal_components/dbgrid1.js', 'sales/proposal_components/dbgrid1-editmenu');
        $this->data['client_dbinput'] = $this->dbinput->render('client_dbinput', 'Pelanggan', 'client_id', 'sales/proposal_components/client_dbinput.js', 'sales/proposal_components/client_dbinput-events.js');
        $this->data['salesperson_dbinput'] = $this->dbinput->render('salesperson_dbinput', 'Penjual', 'salesperson_id', 'sales/proposal_components/salesperson_dbinput.js', 'sales/proposal_components/salesperson_dbinput-events.js');
        $this->data['project_dbinput'] = $this->dbinput->render('project_dbinput', 'Proyek', 'project_id', 'sales/proposal_components/project_dbinput.js', 'sales/proposal_components/project_dbinput-events.js');
        $this->data = array_merge($this->data, $this->jquery->render());
        $this->render($this->data);
    }

}