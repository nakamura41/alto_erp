<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Proposal extends MY_Controller {

    public function index() {
        $this->render($this->data);
    }

    public function clients() {
        $this->load->library('dbgrid', array($this));
        $this->data['dbgrid1'] = $this->dbgrid->render('dbgrid1', 'lists/clients_components/dbgrid1.js', 'lists/clients_components/dbgrid1-editmenu');
        $this->data = array_merge($this->data, $this->jquery->render());
        $this->render($this->data);
    }

}