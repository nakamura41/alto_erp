<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Datasource_static extends CI_Controller {

    // unit

    public function unit_get() {
        $this->load->model('Unit', '', TRUE);
        header('Content-Type: application/json');
        echo json_encode($this->Unit->get());
    }

}