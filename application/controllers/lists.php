<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lists extends MY_Controller {

    public function index() {
        $this->render($this->data);
    }

    public function clients() {
        $this->load->library('dbgrid', array($this));
        $this->data['dbgrid1'] = $this->dbgrid->render('dbgrid1', 'lists/clients_components/dbgrid1.js', 'lists/clients_components/dbgrid1-editmenu');
        $this->data = array_merge($this->data, $this->jquery->render());
        $this->render($this->data);
    }

    public function salespersons() {
        $this->load->library('dbgrid', array($this));
        $this->data['dbgrid1'] = $this->dbgrid->render('dbgrid1', 'lists/salespersons_components/dbgrid1.js', 'lists/salespersons_components/dbgrid1-editmenu');
        $this->data = array_merge($this->data, $this->jquery->render());
        $this->render($this->data);
    }

    public function projects() {
        $this->load->library('dbgrid', array($this));
        $this->data['dbgrid1'] = $this->dbgrid->render('dbgrid1', 'lists/projects_components/dbgrid1.js', 'lists/projects_components/dbgrid1-editmenu');
        $this->data = array_merge($this->data, $this->jquery->render());
        $this->render($this->data);
    }

    public function items() {
        $this->load->library('dbgrid', array($this));
        $this->load->library('dbdropdownlist', array($this));
        $this->data['dbgrid1'] = $this->dbgrid->render('dbgrid1', 'lists/items_components/dbgrid1.js', 'lists/items_components/dbgrid1-editmenu');
        $this->data['unit'] = $this->dbdropdownlist->render('unit', 'lists/items_components/dropdownlist1.js', 'lists/items_components/dropdownlist1-events.js');
        $this->data = array_merge($this->data, $this->jquery->render());
        $this->render($this->data);
    }

    public function supply() {
        $this->load->library('dbgrid', array($this));
        $this->load->library('dbdropdownlist', array($this));
        $this->data['dbgrid1'] = $this->dbgrid->render('dbgrid1', 'lists/supply_components/dbgrid1.js', 'lists/supply_components/dbgrid1-editmenu');
        $this->data['width_unit'] = $this->dbdropdownlist->render('width_unit', 'lists/supply_components/dropdownlist1.js', 'lists/supply_components/dropdownlist1-events.js');
        $this->data['height_unit'] = $this->dbdropdownlist->render('height_unit', 'lists/supply_components/dropdownlist2.js', 'lists/supply_components/dropdownlist2-events.js');
        $this->data['length_unit'] = $this->dbdropdownlist->render('length_unit', 'lists/supply_components/dropdownlist3.js', 'lists/supply_components/dropdownlist3-events.js');
        $this->data['sale_price_unit'] = $this->dbdropdownlist->render('sale_price_unit', 'lists/supply_components/dropdownlist4.js', 'lists/supply_components/dropdownlist4-events.js');
        $this->data = array_merge($this->data, $this->jquery->render());
        $this->render($this->data);
    }

    public function suppliers() {
        $this->load->library('dbgrid', array($this));
        $this->data['dbgrid1'] = $this->dbgrid->render('dbgrid1', 'lists/suppliers_components/dbgrid1.js', 'lists/suppliers_components/dbgrid1-editmenu');
        $this->data = array_merge($this->data, $this->jquery->render());
        $this->render($this->data);
    }

    public function testing() {
        $this->load->library('dbdropdownlist', array($this));
        $this->data['dbdropdownlist1'] = $this->dbdropdownlist->render('dbdropdownlist1', 'lists/supply_components/supply-dropdownlist1.js');
        $this->data = array_merge($this->data, $this->jquery->render());
        $this->render($this->data);
    }

}