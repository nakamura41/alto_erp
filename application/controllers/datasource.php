<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Datasource extends CI_Controller {

    // client methods

    public function clients_get() {
        $this->load->model('Clients', '', TRUE);
        header('Content-Type: application/json');
        echo json_encode($this->Clients->get());
    }

    public function clients_create() {
        $data = $this->input->post('data');
        $this->load->model('Clients', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($data)) {
            echo json_encode($this->Clients->create($data));
        } else {
            echo json_encode(FALSE);
        }
    }

    public function clients_update() {
        $id = $this->input->post('id');
        $data = $this->input->post('data');
        $this->load->model('Clients', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($id) && !empty($data)) {
            echo json_encode($this->Clients->update($id, $data));
        } else {
            echo json_encode(FALSE);
        }
    }

    public function clients_delete() {
        $id = $this->input->post('id');
        $this->load->model('Clients', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($id)) {
            echo json_encode($this->Clients->delete($id));
        } else {
            echo json_encode(FALSE);
        }
    }

    // salesperson methods
    public function salespersons_get() {
        $this->load->model('Salespersons', '', TRUE);
        header('Content-Type: application/json');
        echo json_encode($this->Salespersons->get());
    }

    public function salespersons_create() {
        $data = $this->input->post('data');
        $this->load->model('Salespersons', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($data)) {
            echo json_encode($this->Salespersons->create($data));
        } else {
            echo json_encode(FALSE);
        }
    }

    public function salespersons_update() {
        $id = $this->input->post('id');
        $data = $this->input->post('data');
        $this->load->model('Salespersons', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($id) && !empty($data)) {
            echo json_encode($this->Salespersons->update($id, $data));
        } else {
            echo json_encode(FALSE);
        }
    }

    public function salespersons_delete() {
        $id = $this->input->post('id');
        $this->load->model('Salespersons', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($id)) {
            echo json_encode($this->Salespersons->delete($id));
        } else {
            echo json_encode(FALSE);
        }
    }

    // items methods
    public function items_get() {
        $this->load->model('Items', '', TRUE);
        header('Content-Type: application/json');
        echo json_encode($this->Items->get());
    }

    public function items_create() {
        $data = $this->input->post('data');
        $this->load->model('Items', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($data)) {
            echo json_encode($this->Items->create($data));
        } else {
            echo json_encode(FALSE);
        }
    }

    public function items_update() {
        $id = $this->input->post('id');
        $data = $this->input->post('data');
        $this->load->model('Items', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($id) && !empty($data)) {
            echo json_encode($this->Items->update($id, $data));
        } else {
            echo json_encode(FALSE);
        }
    }

    public function items_delete() {
        $id = $this->input->post('id');
        $this->load->model('Items', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($id)) {
            echo json_encode($this->Items->delete($id));
        } else {
            echo json_encode(FALSE);
        }
    }

    // supply methods
    public function supply_get() {
        $this->load->model('Supply', '', TRUE);
        header('Content-Type: application/json');
        echo json_encode($this->Supply->get());
    }

    public function supply_create() {
        $data = $this->input->post('data');
        $this->load->model('Supply', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($data)) {
            echo json_encode($this->Supply->create($data));
        } else {
            echo json_encode(FALSE);
        }
    }

    public function supply_update() {
        $id = $this->input->post('id');
        $data = $this->input->post('data');
        $this->load->model('Supply', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($id) && !empty($data)) {
            echo json_encode($this->Supply->update($id, $data));
        } else {
            echo json_encode(FALSE);
        }
    }

    public function supply_delete() {
        $id = $this->input->post('id');
        $this->load->model('Supply', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($id)) {
            echo json_encode($this->Supply->delete($id));
        } else {
            echo json_encode(FALSE);
        }
    }

    // suppliers methods
    public function suppliers_get() {
        $this->load->model('Suppliers', '', TRUE);
        header('Content-Type: application/json');
        echo json_encode($this->Suppliers->get());
    }

    public function suppliers_create() {
        $data = $this->input->post('data');
        $this->load->model('Suppliers', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($data)) {
            echo json_encode($this->Suppliers->create($data));
        } else {
            echo json_encode(FALSE);
        }
    }

    public function suppliers_update() {
        $id = $this->input->post('id');
        $data = $this->input->post('data');
        $this->load->model('Suppliers', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($id) && !empty($data)) {
            echo json_encode($this->Suppliers->update($id, $data));
        } else {
            echo json_encode(FALSE);
        }
    }

    public function suppliers_delete() {
        $id = $this->input->post('id');
        $this->load->model('Suppliers', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($id)) {
            echo json_encode($this->Suppliers->delete($id));
        } else {
            echo json_encode(FALSE);
        }
    }

    // projects methods

    public function projects_get() {
        $this->load->model('Projects', '', TRUE);
        header('Content-Type: application/json');
        echo json_encode($this->Projects->get());
    }

    public function projects_create() {
        $data = $this->input->post('data');
        $this->load->model('Projects', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($data)) {
            echo json_encode($this->Projects->create($data));
        } else {
            echo json_encode(FALSE);
        }
    }

    public function projects_update() {
        $id = $this->input->post('id');
        $data = $this->input->post('data');
        $this->load->model('Projects', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($id) && !empty($data)) {
            echo json_encode($this->Projects->update($id, $data));
        } else {
            echo json_encode(FALSE);
        }
    }

    public function projects_delete() {
        $id = $this->input->post('id');
        $this->load->model('Projects', '', TRUE);
        header('Content-Type: application/json');
        if (!empty($id)) {
            echo json_encode($this->Projects->delete($id));
        } else {
            echo json_encode(FALSE);
        }
    }

}